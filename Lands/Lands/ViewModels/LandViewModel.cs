﻿using Lands.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Lands.ViewModels
{
    public class LandViewModel : BaseViewModel
    {
        #region Attributes
        private ObservableCollection<Border> borders;
        private ObservableCollection<Currency> currencyes;
        private ObservableCollection<Language> languajes;
        #endregion
        #region Properties
        public Land Land { get; set; }

        public ObservableCollection<Border> Borders
        {
            get { return borders; }
            set { SetValue(ref borders, value); }
        }

        public ObservableCollection<Currency> Currencyes
        {
            get { return currencyes; }
            set { SetValue(ref currencyes, value); }
        }

        public ObservableCollection<Language> Languajes
        {
            get { return languajes; }
            set { SetValue(ref languajes, value); }
        }
        #endregion

        #region Constructor
        public LandViewModel(Land land)
        {
            this.Land = land;
            LoadBorders();
            Currencyes = new  ObservableCollection<Currency>(Land.Currencies);
            Languajes = new ObservableCollection<Language>(Land.Languages);
        }
        #endregion

        #region Borders
        private void LoadBorders()
        {
            Borders = new ObservableCollection<Border>();
            foreach (var border in Land.Borders)
            {
                var land = MainViewModel.GetInstance().LandList.Where(l => l.Alpha3Code == border).FirstOrDefault();
                if (land != null)
                {
                    Borders.Add(new Border
                    {
                        Code = land.Alpha3Code,
                        Name = land.Name,
                    });
                }
            }
        }
        #endregion

    }
}
